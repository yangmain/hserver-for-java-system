import { VALIDATE_NO_PASSED, getRefPromise, validateFormAndTables } from '@/utils/JEditableTableUtil'
import { httpAction, getAction } from '@/api/manage'

export const JEditableTableMixin = {
  data() {
    return {
      title: '操作',
      visible: false,
      form: this.$form.createForm(this),
      confirmLoading: false,
      model: {},
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      },
      editable:true,
      isview:false
    }
  },
  methods: {
    view(record) {
      if (typeof this.viewBefore === 'function') this.viewBefore(record)
      this.edit(record)
      if (typeof this.viewAfter === 'function') this.viewAfter(this.model)
      this.editable=false
      this.isview=true
      this.title="查看详情"
    },
    /** 获取所有的editableTable实例 */
    getAllTable() {
      if (!(this.refKeys instanceof Array)) {
        throw this.throwNotArray('refKeys')
      }
      let values = this.refKeys.map(key => getRefPromise(this, key))
      return Promise.all(values)
    },

    /** 遍历所有的JEditableTable实例 */
    eachAllTable(callback) {
      // 开始遍历
      this.getAllTable().then(tables => {
        tables.forEach((item, index) => {
          if (typeof callback === 'function') {
            callback(item, index)
          }
        })
      })
    },

    /** 当点击新增按钮时调用此方法 */
    add() {
      if (typeof this.addBefore === 'function') this.addBefore()
      // 默认新增空数据
      this.edit({})
      if (typeof this.addAfter === 'function') this.addAfter(this.model)
    },
    /**用于重新发起 */
    readd(record){
      if (typeof this.editBefore === 'function') this.editBefore(record)
      this.visible = true
      this.editable=true
      this.activeKey = this.refKeys[0]
      this.form.resetFields()
      this.model = Object.assign({}, record)
      if (typeof this.editAfter === 'function') this.editAfter(this.model)
      this.model.id=null
      this.model.status=null
    },
    /** 当点击了编辑（修改）按钮时调用此方法 */
    edit(record) {
      if (typeof this.editBefore === 'function') this.editBefore(record)
      this.visible = true
      this.editable=true
      this.activeKey = this.refKeys[0]
      this.form.resetFields()
      this.model = Object.assign({}, record)
      if (typeof this.editAfter === 'function') this.editAfter(this.model)
    },
    /** 关闭弹窗，并将所有JEditableTable实例回归到初始状态 */
    close() {
      this.visible = false
      this.$emit('close')
    },
    /** 发起请求，自动判断是执行新增还是修改操作 */
    request(formData) {
      let url = this.url.add, method = 'post'
      if (this.model.id) {
        url = this.url.edit
        method = 'put'
      }
      this.confirmLoading = true
      httpAction(url, formData, method).then((res) => {
        if (res.success) {
          this.$message.success(res.msg)
          this.$emit('ok')
          this.close()
        } else {
          this.$message.warning(res.msg)
        }
      }).finally(() => {
        this.confirmLoading = false
      })
    },
    processTableData(arr){
      console.info('process table data');
    },
    /** 查询某个tab的数据 */
    requestSubTableData(url, params, tab) {
      tab.loading = true
      getAction(url, params).then(res => {
        this.processTableData(res.result)
        tab.dataSource = res.result || []
      }).finally(() => {
        tab.loading = false
      })
    },
    /** 关闭按钮点击事件 */
    handleCancel() {
      this.close()
    },
    handleChangeTabs(){},
    /** 确定按钮点击事件 */
    handleOk() {
      if(!this.editable){
        this.close()
        return
      }
      if (typeof this.classifyIntoFormData !== 'function') {
        throw this.throwNotFunction('classifyIntoFormData')
      }
      let formData = this.classifyIntoFormData()
      formData=Object.assign(this.model,formData)
      // 发起请求
      return this.request(formData)
      
    },

    /* --- throw --- */

    /** not a function */
    throwNotFunction(name) {
      return `${name} 未定义或不是一个函数`
    },
    /** not a array */
    throwNotArray(name) {
      return `${name} 未定义或不是一个数组`
    }

  }
}