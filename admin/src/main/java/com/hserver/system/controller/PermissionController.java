package com.hserver.system.controller;

import com.hserver.system.entity.PermissionEntity;
import com.hserver.system.service.PermissionService;
import com.hserver.system.service.RoleService;
import com.hserver.system.service.TokenService;
import com.hserver.system.service.UserService;
import top.hserver.core.interfaces.PermissionAdapter;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.server.router.RouterPermission;
import top.hserver.core.server.util.JsonResult;

import java.util.*;

@Controller
public class PermissionController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RoleService roleService;

    @GET("/admin/permission/getUserPermissionByToken")
    @POST("/admin/permission/getUserPermissionByToken")
    public JsonResult getUserPermissionByToken(String token) {
        return permissionService.getUserPermissionByToken(token);
    }

    @RequiresPermissions("查询角色权限")
    @GET("/admin/permission/queryRolePermission")
    public JsonResult queryRolePermission(Map params) {
        List<Integer> ids = permissionService.getIdsByRoleId(Integer.valueOf(params.get("roleId").toString()));
        List sids = new ArrayList();
        if (ids != null) {
            for (int i = 0; i < ids.size(); i++) {
                sids.add(ids.get(i).toString());
            }
        }
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        jsonResult.put("result", sids);
        return jsonResult;
    }

    @RequiresPermissions("保存角色权限")
    @POST("/admin/permission/saveRolePermission")
    public JsonResult saveRolePermission(Map params) {
        String roleId = params.get("roleId").toString();
        String permissionIds = params.get("permissionIds").toString();
        Integer r = Integer.valueOf(roleId);
        String[] pIds = permissionIds.split(",");
        permissionService.deleteRolePermission(r);
        for (int i = 0; i < pIds.length; i++) {
            permissionService.saveRolePermission(r, Integer.valueOf(pIds[i]));
        }
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        return jsonResult;
    }

    @RequiresPermissions("权限列表")
    @GET("/admin/permission/list")
    public JsonResult list(PermissionEntity permissionEntity) {
        JsonResult jsonResult = JsonResult.ok();
        List pageView = permissionService.page(permissionEntity);
        jsonResult.put("result", pageView);
        jsonResult.put("success", true);
        return jsonResult;
    }

    @RequiresPermissions("添加菜单或权限")
    @POST("/admin/permission/add")
    public JsonResult add(PermissionEntity permissionEntity) {
        JsonResult jsonResult = permissionService.add(permissionEntity);
        return jsonResult;
    }

    @RequiresPermissions("编辑菜单或权限")
    @POST("/admin/permission/edit")
    public JsonResult edit(PermissionEntity permissionEntity) {
        return permissionService.edit(permissionEntity);
    }

    @RequiresPermissions( "删除菜单或权限")
    @GET("/admin/permission/delete")
    public JsonResult delete(Integer id) {
        return permissionService.deleteById(Integer.valueOf(id));
    }

    @RequiresPermissions("批量删除菜单或权限")
    @GET("/admin/permission/deleteBatch")
    public JsonResult deleteBatch(String ids) {
        String[] id = ids.split(",");
        return permissionService.deleteBatch(id);
    }

    @GET("/admin/permission/getPermRuleListByPermId")
    public JsonResult getPermRuleListByPermId(String id) {
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        jsonResult.put("result", new ArrayList<>());
        return jsonResult;
    }

    @RequiresPermissions("查询菜单或权限")
    @GET("/admin/permission/queryTreeList")
    public JsonResult queryTreeList(String ids) {
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        List<PermissionEntity> permissionEntities = roleService.queryTreeList();
        Map treeList = new HashMap();
        treeList.put("treeList", permissionEntities);
        //List<Integer> ids=roleService.getRolePermissionIds();
        treeList.put("ids", new ArrayList<>());
        jsonResult.put("result", treeList);
        return jsonResult;
    }


    /**
     * 同步方法到权限里面
     *
     * @return
     */
    @GET("/admin/permission/synchronize")
    public JsonResult synchronize() {
        List<RouterPermission> routerPermissions = PermissionAdapter.getRouterPermissions();

        for (RouterPermission routerPermission : routerPermissions) {
            if (routerPermission.getControllerPackageName() != null && routerPermission.getControllerPackageName().trim().length() > 0) {
                permissionService.synchronize(routerPermission.getControllerPackageName());
            }
        }
        Map<String, Integer> map = new HashMap<>();
        for (RouterPermission v : routerPermissions) {
            if (v.getControllerPackageName() != null && v.getControllerPackageName().trim().length() > 0) {
                Integer integer1 = map.get(v.getControllerPackageName());
                if (integer1 == null) {
                    Integer integer = permissionService.synchronizeId(v.getControllerPackageName());
                    map.put(v.getControllerPackageName(), integer);
                }
            }

        }
        for (RouterPermission v : routerPermissions) {
            PermissionEntity permissionEntity = new PermissionEntity();
            StringBuffer stringBuffer = new StringBuffer();
            for (String s : v.getRequiresPermissions().value()) {
                stringBuffer.append(s);
                stringBuffer.append(",");
            }
            permissionEntity.setName(stringBuffer.toString());
            permissionEntity.setRoute(false);
            permissionEntity.setPath(v.getUrl());
            permissionEntity.setMenuType(2);
            permissionEntity.setSortNo(20);
            permissionEntity.setParentId(map.get(v.getControllerPackageName()));
            permissionService.synchronize(permissionEntity);
        }
        return JsonResult.ok();
    }


}
