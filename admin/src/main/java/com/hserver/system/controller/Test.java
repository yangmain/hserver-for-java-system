package com.hserver.system.controller;

import com.hserver.system.service.UserService;
import com.hserver.system.utils.RedisUtil;
import redis.clients.jedis.Jedis;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.RequiresPermissions;
import top.hserver.core.server.util.JsonResult;

@Controller
public class Test {

    @Autowired
    private RedisUtil redisUtil;

    @RequiresPermissions("hello测试")
    @GET("/hello")
    public JsonResult hello() {
        return JsonResult.ok(redisUtil.get("user:token:admin" ));
    }
}
