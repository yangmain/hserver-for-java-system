package com.hserver.system.filter;

import com.hserver.system.service.TokenService;
import top.hserver.core.interfaces.FilterAdapter;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Filter;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.filter.FilterChain;

@Filter(1)
public class GlobalPermissionFilter implements FilterAdapter {

    private String login="/admin/login";

    @Autowired
    private TokenService tokenService;
    @Override
    public void doFilter(FilterChain chain, Webkit webkit) throws Exception {
        if(webkit.httpRequest.getUri().startsWith(login)){
            chain.doFilter(webkit);
        }else {
            chain.doFilter(webkit);
            /*
            String token = webkit.httpRequest.getHeader("x-access-token");
            if (token != null) {
                if (tokenService.hasUser(token)) {
                    chain.doFilter(webkit);
                } else {
                    throw new RuntimeException("用户未登录");
                }
            } else {
                throw new RuntimeException("用户未登录");
            }
            */
        }
    }
}