package com.hserver.system.utils;


import java.util.List;

public class PageView<T> {

    private Integer total;

    private Integer size;

    private Integer current;

    private List<T> records;

    private Integer pages;

    public static PageView of(int total,int size,int page,List data){
        PageView pageView=new PageView();
        pageView.total=total;
        pageView.records=data;
        pageView.size=size;
        pageView.current=page;
        pageView.pages=page;
        return  pageView;
    }

  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getCurrent() {
    return current;
  }

  public void setCurrent(Integer current) {
    this.current = current;
  }

  public List<T> getRecords() {
    return records;
  }

  public void setRecords(List<T> records) {
    this.records = records;
  }

  public Integer getPages() {
    return pages;
  }

  public void setPages(Integer pages) {
    this.pages = pages;
  }
}
