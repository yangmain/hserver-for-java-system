package com.hserver.system.mapper;

import com.hserver.system.entity.PermissionEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PermissionMapper {


    List<PermissionEntity> list(@Param("p")PermissionEntity permissionEntity);

    PermissionEntity getByNameOne(@Param("name")String name);

    @Select("select * from sys_permission r join sys_role_permission rp on rp.permission_id = r.id where rp.role_id  = #{p}")
    List<Integer> getIdsByRoleId(@Param("p") Integer valueOf);

    @Delete("delete from sys_role_permission where role_id = #{p}")
    void deleteRolePermissionByRoleId(@Param("p") Integer r);

    @Insert("insert into sys_role_permission(role_id,permission_id) value(#{p},#{p1})")
    void saveRolePermission(@Param("p")Integer r, @Param("p1")Integer valueOf);

    int cont(@Param("p")PermissionEntity permissionEntity);

    @Insert("insert into sys_permission(parent_id,name,url,component,redirect,menu_type,sort_no,icon,is_route,description,path)" +
            "  value(#{p.parentId},#{p.name},#{p.url},#{p.component},#{p.redirect},#{p.menuType},#{p.sortNo},#{p.icon},#{p.isRoute},#{p.description},#{p.path})")
    int insert(@Param("p")PermissionEntity permissionEntity);

    @Update("update sys_permission set parent_id = #{p.parentId},name=#{p.name},url=#{p.url},component=#{p.component},redirect=#{p.redirect},menu_type =#{p.menuType},sort_no=#{p.sortNo},icon=#{p.icon},is_route=#{p.isRoute},description=#{p.description},path=#{p.path} where id=#{p.id}")
    void update(@Param("p")PermissionEntity permissionEntity);

    @Delete("delete from sys_permission where id=#{p}")
    void deleteById(@Param("p")Integer id);

}
