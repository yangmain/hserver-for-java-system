package com.hserver.system.mapper;

import com.hserver.system.entity.RoleEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface RoleMapper {

  List<RoleEntity> list(@Param("p") RoleEntity roleEntity);

  int cont(@Param("p") RoleEntity roleEntity);

  @Options(useGeneratedKeys = true)
  @Insert("insert into sys_role (role_name,role_code,description)" +
    " values(#{p.roleName},#{p.roleCode},#{p.description})")
  int insert(@Param("p") RoleEntity roleEntity);

  @Update("update sys_role set role_name=#{p.roleName},role_code=#{p.roleCode},description=#{p.description} where id=#{p.id}")
  void update(@Param("p") RoleEntity roleEntity);

  @Delete("delete from sys_role where id = #{p}")
  void deleteById(@Param("p") Integer id);

}
