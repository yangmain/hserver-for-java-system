package com.hserver.system.mapper;

import com.hserver.system.entity.TokenEntity;
import org.apache.ibatis.annotations.*;

public interface TokenMapper {

    @Select("select * from sys_token where create_by = #{p}")
    TokenEntity getToken(@Param("p")String username);

    @Options(useGeneratedKeys = true)
    @Insert("insert into sys_token(token,create_by,create_time) value(#{p.token},#{p.createBy},#{p.createTime})")
    int saveToken(@Param("p")TokenEntity tokenEntity);

    @Delete("delete from sys_token where create_by = #{p}")
    void deleteByUsername(@Param("p")String username);

    @Select("select * from sys_token where token = #{p}")
    TokenEntity getByToken(@Param("p")String st);
}
