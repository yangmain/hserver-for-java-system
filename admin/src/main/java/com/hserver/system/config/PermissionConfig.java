package com.hserver.system.config;

import com.hserver.system.service.TokenService;
import redis.clients.jedis.Jedis;
import top.hserver.core.interfaces.PermissionAdapter;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.util.JsonResult;

import java.util.Map;

@Bean
public class PermissionConfig implements PermissionAdapter {
    @Autowired
    private TokenService tokenService;

    @Override
    public void requiresPermissions(RequiresPermissions requiresPermissions, Webkit webkit) throws Exception {
        String token = webkit.httpRequest.getHeader("x-access-token");
        if (token == null) {
            webkit.httpResponse.sendJson(JsonResult.error("x-access-token：token不存在"));
        } else {
            if (!tokenService.requiresPermissions(token, webkit)){
                webkit.httpResponse.sendJson(JsonResult.error("token验证无效！"));
            }
        }
    }

    @Override
    public void requiresRoles(RequiresRoles requiresRoles, Webkit webkit)  throws Exception{
        //这里你可以实现一套自己的角色检查算法逻辑，判断，
        //其他逻辑同上
        System.out.println(requiresRoles.value()[0]);
    }

    @Override
    public void sign(Sign sign, Webkit webkit)  throws Exception{
        //这里你可以实现一套自己的接口签名算法检查算法逻辑，判断，
        //其他逻辑同上
        Map requestParams = webkit.httpRequest.getRequestParams();
        String sign1 = webkit.httpRequest.getHeader("sign");
        System.out.println(sign.value());
    }
}
