package com.hserver.system.config;

import top.hserver.core.interfaces.GlobalException;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.util.JsonResult;

@Bean
public class WebException implements GlobalException {

    @Override
    public void handler(Exception exception, Webkit webkit) {
        exception.printStackTrace();
        System.out.println(webkit.httpRequest.getUri() + "--->" + exception.getMessage());
        JsonResult result =JsonResult.error("发生了一个错误");
        result.put("success",false);
        webkit.httpResponse.sendJson(result);
    }
}