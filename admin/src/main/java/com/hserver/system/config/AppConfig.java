package com.hserver.system.config;

import com.hserver.system.entity.ConfigEntity;
import com.hserver.system.utils.RedisUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import redis.clients.jedis.Jedis;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Configuration;
import top.hserver.core.properties.PropertiesInit;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class AppConfig {

    public static ConfigEntity configEntity;

    static {
        Properties pps = new Properties();
        InputStream resourceAsStream = PropertiesInit.class.getResourceAsStream("/application.properties");
        try {
            pps.load(resourceAsStream);
            configEntity = new ConfigEntity();
            Object token_localhost = pps.get("token_localhost");
            Object token_expire = pps.get("token_expire");
            if (token_expire != null && token_expire.toString().trim().length() > 0) {
                int i = Integer.parseInt(token_expire.toString());
                configEntity.setTokenExpire(i);
            } else {
                configEntity = null;
            }
            if (token_localhost != null && token_localhost.toString().trim().length() > 0) {
                configEntity.setTokenLocation(token_localhost.toString());
            } else {
                configEntity = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Bean
    public SqlSessionFactory sqlSessionFactory() throws IOException {
        // 指定全局配置文件
        String resource = "mybatis-config.xml";
        // 读取配置文件
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 构建sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "development");
        return sqlSessionFactory;
    }

    @Bean
    public RedisUtil getJedis() {
        return RedisUtil.getRedisUtil("127.0.0.1",6379,"123456a.");
    }
}
