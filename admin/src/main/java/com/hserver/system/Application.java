package com.hserver.system;

import top.hserver.HServerApplication;

public class Application {
    public static void main(String[] args) {
        //运行官方例子,直接运行既可以了，默认自带了一些例子。
        //这样运行，直接启动hserver内部测试的程序,地址栏直接运行即可
        HServerApplication.run(Application.class, 8888);
    }
}
