package com.hserver.system.service;

import com.hserver.system.entity.ConfigEntity;
import top.hserver.core.ioc.annotation.Bean;

import static com.hserver.system.config.AppConfig.configEntity;

@Bean
public class ConfigService {

    public String getTokenLocation() {
        return configEntity.getTokenLocation();
    }

    public ConfigEntity getConfig() {
        return configEntity;
    }

    public void edit(ConfigEntity configEntityMe) {
        configEntity = configEntityMe;
    }
}
