package com.hserver.system.entity;


public class ViewModel {

  private Integer pageNo;

  private Integer pageSize = 10;

  /**
   * page offset
   */
  private Integer poff;

  public Integer getPoff() {
    if (pageNo != null && pageSize != null) {
      if (pageNo > 0) {
        pageNo = pageNo - 1;
      }
      return pageNo * pageSize;
    } else {
      return null;
    }
  }


  public Integer getPageNo() {
    return pageNo;
  }

  public void setPageNo(Integer pageNo) {
    this.pageNo = pageNo;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public void setPoff(Integer poff) {
    this.poff = poff;
  }
}
