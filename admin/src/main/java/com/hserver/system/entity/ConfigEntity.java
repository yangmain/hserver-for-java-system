package com.hserver.system.entity;

public class ConfigEntity extends BaseEntity {

  private String tokenLocation;

  /**
   * token过期时长
   */
  private Integer tokenExpire;

  public String getTokenLocation() {
    return tokenLocation;
  }

  public void setTokenLocation(String tokenLocation) {
    this.tokenLocation = tokenLocation;
  }

  public Integer getTokenExpire() {
    return tokenExpire;
  }

  public void setTokenExpire(Integer tokenExpire) {
    this.tokenExpire = tokenExpire;
  }
}
