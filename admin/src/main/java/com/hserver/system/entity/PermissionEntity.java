package com.hserver.system.entity;


import java.util.List;
import java.util.Map;

public class PermissionEntity extends BaseEntity {

  private Integer key;

  private Integer parentId;
  private String name;
  private String url;
  private String path;
  private String component;
  private String componentName;
  private String redirect;
  private Integer menuType;
  private String perms;
  private Integer sortNo;
  private String icon;
  private Boolean isRoute;
  private String description;

  private Map meta;

  private List<PermissionEntity> children;

  public Integer getKey() {
    return key;
  }

  public void setKey(Integer key) {
    this.key = key;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getComponent() {
    return component;
  }

  public void setComponent(String component) {
    this.component = component;
  }

  public String getComponentName() {
    return componentName;
  }

  public void setComponentName(String componentName) {
    this.componentName = componentName;
  }

  public String getRedirect() {
    return redirect;
  }

  public void setRedirect(String redirect) {
    this.redirect = redirect;
  }

  public Integer getMenuType() {
    return menuType;
  }

  public void setMenuType(Integer menuType) {
    this.menuType = menuType;
  }

  public String getPerms() {
    return perms;
  }

  public void setPerms(String perms) {
    this.perms = perms;
  }

  public Integer getSortNo() {
    return sortNo;
  }

  public void setSortNo(Integer sortNo) {
    this.sortNo = sortNo;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Boolean getRoute() {
    return isRoute;
  }

  public void setRoute(Boolean route) {
    isRoute = route;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Map getMeta() {
    return meta;
  }

  public void setMeta(Map meta) {
    this.meta = meta;
  }

  public List<PermissionEntity> getChildren() {
    return children;
  }

  public void setChildren(List<PermissionEntity> children) {
    this.children = children;
  }
}
