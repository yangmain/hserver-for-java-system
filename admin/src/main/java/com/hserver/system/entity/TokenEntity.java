package com.hserver.system.entity;

public class TokenEntity extends BaseEntity {
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
