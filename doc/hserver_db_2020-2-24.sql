/*
 Navicat Premium Data Transfer

 Source Server         : 5.7
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : hserver_db

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 24/02/2020 22:26:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路径',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单权限编码',
  `sort_no` int(10) DEFAULT NULL COMMENT '菜单排序',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) DEFAULT 1 COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT 0 COMMENT '删除状态 0正常 1已删除',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_prem_pid`(`parent_id`) USING BTREE,
  INDEX `index_prem_sort_no`(`sort_no`) USING BTREE,
  INDEX `index_prem_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, NULL, '首页', '/dashboard/analysis', 'dashboard/Analysis', NULL, NULL, 0, NULL, 0, 'home', 1, NULL, NULL, NULL, NULL, NULL, 0, '/dashboard/analysis');
INSERT INTO `sys_permission` VALUES (3, 7, '用户管理', '/isystem/user', 'system/UserList', NULL, NULL, 1, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/user');
INSERT INTO `sys_permission` VALUES (4, 7, '菜单管理', '/isystem/newPermissionList', 'system/PermissionList', NULL, NULL, 1, NULL, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/newPermissionList');
INSERT INTO `sys_permission` VALUES (5, 7, '角色管理', '/isystem/roleUserList', 'system/RoleList', NULL, NULL, 1, NULL, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/roleUserList');
INSERT INTO `sys_permission` VALUES (7, NULL, '系统管理', '/isystem', 'layouts/RouteView', NULL, NULL, 0, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem');
INSERT INTO `sys_permission` VALUES (9, 7, '基本配置', '/isystem/confg', 'system/Config', NULL, NULL, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/confg');
INSERT INTO `sys_permission` VALUES (11, NULL, '测试菜单', '/test/t', 'test/T', NULL, NULL, 0, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/test/t');
INSERT INTO `sys_permission` VALUES (12, NULL, 'com.hserver.system.controller.PermissionController', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (13, NULL, 'com.hserver.system.controller.UserController', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (14, NULL, 'com.hserver.system.controller.RoleController', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (15, NULL, 'com.hserver.system.controller.ConfigController', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (16, 12, '批量删除菜单或权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/deleteBatch');
INSERT INTO `sys_permission` VALUES (17, 13, '删除用户,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/delete');
INSERT INTO `sys_permission` VALUES (18, 14, '查询所有角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/queryall');
INSERT INTO `sys_permission` VALUES (19, 13, '检查用户是否存在,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/checkOnlyUser');
INSERT INTO `sys_permission` VALUES (20, 14, '删除角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/delete');
INSERT INTO `sys_permission` VALUES (21, 14, '批量角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/deleteBatch');
INSERT INTO `sys_permission` VALUES (22, 12, '删除菜单或权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/delete');
INSERT INTO `sys_permission` VALUES (23, 12, '查询角色权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/queryRolePermission');
INSERT INTO `sys_permission` VALUES (24, 14, '查询角色信息,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/queryTreeList');
INSERT INTO `sys_permission` VALUES (25, 13, '批量删除用户,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/deleteBatch');
INSERT INTO `sys_permission` VALUES (26, 13, '查询用户角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/queryUserRole');
INSERT INTO `sys_permission` VALUES (27, 13, '用户列表,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/list');
INSERT INTO `sys_permission` VALUES (28, 14, '角色列表,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/list');
INSERT INTO `sys_permission` VALUES (29, 12, '权限列表,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/list');
INSERT INTO `sys_permission` VALUES (30, 12, '查询菜单或权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/queryTreeList');
INSERT INTO `sys_permission` VALUES (31, 15, '缓存获取,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/config/get');
INSERT INTO `sys_permission` VALUES (32, 13, '添加用户,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/add');
INSERT INTO `sys_permission` VALUES (33, 12, '编辑菜单或权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/edit');
INSERT INTO `sys_permission` VALUES (34, 14, '编辑角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/edit');
INSERT INTO `sys_permission` VALUES (35, 15, '缓存编辑,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/config/edit');
INSERT INTO `sys_permission` VALUES (36, 12, '保存角色权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/saveRolePermission');
INSERT INTO `sys_permission` VALUES (37, 13, '编辑用户,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/edit');
INSERT INTO `sys_permission` VALUES (38, 12, '添加菜单或权限,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/add');
INSERT INTO `sys_permission` VALUES (39, 14, '添加角色,', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/add');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', '管理员角色', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_group_role_per_id`(`role_id`, `permission_id`) USING BTREE,
  INDEX `index_group_role_id`(`role_id`) USING BTREE,
  INDEX `index_group_per_id`(`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (13, 1, 1, NULL);
INSERT INTO `sys_role_permission` VALUES (14, 1, 7, NULL);
INSERT INTO `sys_role_permission` VALUES (15, 1, 3, NULL);
INSERT INTO `sys_role_permission` VALUES (16, 1, 4, NULL);
INSERT INTO `sys_role_permission` VALUES (17, 1, 5, NULL);
INSERT INTO `sys_role_permission` VALUES (18, 1, 11, NULL);
INSERT INTO `sys_role_permission` VALUES (19, 1, 9, NULL);
INSERT INTO `sys_role_permission` VALUES (20, 1, 12, NULL);
INSERT INTO `sys_role_permission` VALUES (21, 1, 13, NULL);
INSERT INTO `sys_role_permission` VALUES (22, 1, 14, NULL);
INSERT INTO `sys_role_permission` VALUES (23, 1, 15, NULL);
INSERT INTO `sys_role_permission` VALUES (24, 1, 16, NULL);
INSERT INTO `sys_role_permission` VALUES (25, 1, 17, NULL);
INSERT INTO `sys_role_permission` VALUES (26, 1, 18, NULL);
INSERT INTO `sys_role_permission` VALUES (27, 1, 19, NULL);
INSERT INTO `sys_role_permission` VALUES (28, 1, 20, NULL);
INSERT INTO `sys_role_permission` VALUES (29, 1, 21, NULL);
INSERT INTO `sys_role_permission` VALUES (30, 1, 22, NULL);
INSERT INTO `sys_role_permission` VALUES (31, 1, 23, NULL);
INSERT INTO `sys_role_permission` VALUES (32, 1, 24, NULL);
INSERT INTO `sys_role_permission` VALUES (33, 1, 25, NULL);
INSERT INTO `sys_role_permission` VALUES (34, 1, 26, NULL);
INSERT INTO `sys_role_permission` VALUES (35, 1, 27, NULL);
INSERT INTO `sys_role_permission` VALUES (36, 1, 28, NULL);
INSERT INTO `sys_role_permission` VALUES (37, 1, 29, NULL);
INSERT INTO `sys_role_permission` VALUES (38, 1, 30, NULL);
INSERT INTO `sys_role_permission` VALUES (39, 1, 31, NULL);
INSERT INTO `sys_role_permission` VALUES (40, 1, 32, NULL);
INSERT INTO `sys_role_permission` VALUES (41, 1, 33, NULL);
INSERT INTO `sys_role_permission` VALUES (42, 1, 34, NULL);
INSERT INTO `sys_role_permission` VALUES (43, 1, 35, NULL);
INSERT INTO `sys_role_permission` VALUES (44, 1, 36, NULL);
INSERT INTO `sys_role_permission` VALUES (45, 1, 37, NULL);
INSERT INTO `sys_role_permission` VALUES (46, 1, 38, NULL);
INSERT INTO `sys_role_permission` VALUES (47, 1, 39, NULL);

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'token',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT 0 COMMENT '删除状态 0正常 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_token
-- ----------------------------
INSERT INTO `sys_token` VALUES (86, 'c09cd5324e91428a8c3e40dd3abb33ad', NULL, 'admin', '2020-02-24 14:07:09', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `birthday` datetime(0) DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话',
  `status` int(2) DEFAULT NULL COMMENT '状态(1：正常  2：冻结 ）',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_user_name`(`username`) USING BTREE,
  INDEX `index_user_status`(`status`) USING BTREE,
  INDEX `index_user_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'admin', 'admin', NULL, NULL, NULL, '13064001744', NULL, '0', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index2_groupuu_user_id`(`user_id`) USING BTREE,
  INDEX `index2_groupuu_ole_id`(`role_id`) USING BTREE,
  INDEX `index2_groupuu_useridandroleid`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (5, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
