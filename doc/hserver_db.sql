﻿# Host: 115.29.100.191  (Version 5.7.29-0ubuntu0.16.04.1)
# Date: 2020-02-12 14:34:00
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "sys_config"
#

CREATE TABLE `sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `token_location` varchar(255) DEFAULT NULL COMMENT 'token存放位置',
  `token_expire` int(11) DEFAULT NULL COMMENT 'token存储时间（秒）',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除状态 0正常 1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='配置表';

#
# Data for table "sys_config"
#

INSERT INTO `sys_config` VALUES (1,'redis',120,NULL,NULL,NULL,NULL,NULL,0);

#
# Structure for table "sys_permission"
#

CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) DEFAULT NULL COMMENT '菜单权限编码',
  `sort_no` int(10) DEFAULT NULL COMMENT '菜单排序',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) DEFAULT '1' COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除状态 0正常 1已删除',
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_prem_pid` (`parent_id`) USING BTREE,
  KEY `index_prem_sort_no` (`sort_no`) USING BTREE,
  KEY `index_prem_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

#
# Data for table "sys_permission"
#

INSERT INTO `sys_permission` VALUES (1,NULL,'首页','/dashboard/analysis','dashboard/Analysis',NULL,NULL,0,NULL,0,'home',1,NULL,NULL,NULL,NULL,NULL,0,'/dashboard/analysis'),(3,7,'用户管理','/isystem/user','system/UserList',NULL,NULL,1,NULL,2,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/isystem/user'),(4,7,'菜单管理','/isystem/newPermissionList','system/PermissionList',NULL,NULL,1,NULL,3,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/isystem/newPermissionList'),(5,7,'角色管理','/isystem/roleUserList','system/RoleList',NULL,NULL,1,NULL,4,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/isystem/roleUserList'),(7,NULL,'系统管理','/isystem','layouts/RouteView',NULL,NULL,0,NULL,1,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/isystem'),(9,7,'基本配置','/isystem/confg','system/Config',NULL,NULL,1,NULL,1,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/isystem/confg'),(11,NULL,'测试菜单','/test/t','test/T',NULL,NULL,0,NULL,1,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'/test/t');

#
# Structure for table "sys_role"
#

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_name` varchar(200) DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) NOT NULL COMMENT '角色编码',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='角色表';

#
# Data for table "sys_role"
#

INSERT INTO `sys_role` VALUES (1,'管理员','admin','管理员角色',NULL,NULL,NULL,NULL),(3,'111','1','1',NULL,NULL,NULL,NULL);

#
# Structure for table "sys_role_permission"
#

CREATE TABLE `sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_group_role_per_id` (`role_id`,`permission_id`) USING BTREE,
  KEY `index_group_role_id` (`role_id`) USING BTREE,
  KEY `index_group_per_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='角色权限表';

#
# Data for table "sys_role_permission"
#

INSERT INTO `sys_role_permission` VALUES (10,3,3,NULL),(11,3,7,NULL),(12,3,9,NULL),(13,1,1,NULL),(14,1,7,NULL),(15,1,3,NULL),(16,1,4,NULL),(17,1,5,NULL),(18,1,11,NULL);

#
# Structure for table "sys_token"
#

CREATE TABLE `sys_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除状态 0正常 1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COMMENT='token表';

#
# Data for table "sys_token"
#

INSERT INTO `sys_token` VALUES (62,'036232467c804cfda322c2c6ea8cbd17',NULL,'a','2020-02-11 05:13:31',NULL,NULL,0),(85,'cf7cceeb473541e6a03451aa959aedaa',NULL,'admin','2020-02-12 00:21:35',NULL,NULL,0);

#
# Structure for table "sys_user"
#

CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(100) DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `status` int(2) DEFAULT NULL COMMENT '状态(1：正常  2：冻结 ）',
  `del_flag` varchar(1) DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_name` (`username`) USING BTREE,
  KEY `index_user_status` (`status`) USING BTREE,
  KEY `index_user_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='用户表';

#
# Data for table "sys_user"
#

INSERT INTO `sys_user` VALUES (1,'admin','admin','admin',NULL,NULL,NULL,'13064001744',NULL,'0',NULL,NULL,NULL,NULL),(22,'ss','ss','ss',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'test','test','test',NULL,1,'47@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "sys_user_role"
#

CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`),
  KEY `index2_groupuu_user_id` (`user_id`) USING BTREE,
  KEY `index2_groupuu_ole_id` (`role_id`) USING BTREE,
  KEY `index2_groupuu_useridandroleid` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

#
# Data for table "sys_user_role"
#

INSERT INTO `sys_user_role` VALUES (5,1,1),(6,1,3),(9,1,3),(10,1,3),(11,1,3),(12,1,4),(13,1,3),(14,1,4),(15,1,3),(16,1,4),(17,1,3),(18,1,4),(19,21,3),(20,21,4),(21,22,3),(22,22,4),(23,11,3),(24,23,1);
